import sys

from hw03 import task17, task18, task19, task20, task21, task22, task23, task24
from hw04 import task25, task26, task27, task28, task29
from make import register_targets, file_to_args, make


def imports():
    from hw04.task30 import get_targets
    from hw05.task32 import get_targets
    from hw05.task33 import get_targets
    from hw05.task34 import get_targets
    from hw05.task35 import get_targets
    from hw05.task36 import get_targets
    from hw05.task37 import get_targets
    from hw05.task38 import get_targets
    from hw05.task39 import get_targets
    from hw06.task40 import get_targets
    from hw06.task41 import get_targets
    from hw06.task42 import get_targets
    from hw07.task43 import get_targets


@register_targets
def targets():
    out = dict()
    out['task17_00'] = (task17.main, file_to_args('hw03/in17_00.txt'))
    out['task17_01'] = (task17.main, file_to_args('hw03/in17_01.txt'))
    out['task18_00'] = (task18.main, file_to_args('hw03/in18_00.txt'))
    out['task18_01'] = (task18.main, file_to_args('hw03/in18_01.txt'))
    out['task19_00'] = (task19.main, file_to_args('hw03/in19_00.txt'))
    out['task19_01'] = (task19.main, file_to_args('hw03/in19_01.txt'))
    out['task20_00'] = (task20.main, file_to_args('hw03/in20_00.txt'))
    out['task20_01'] = (task20.main, file_to_args('hw03/in20_01.txt'))
    out['task21_00'] = (task21.main, file_to_args('hw03/in21_00.txt'))
    out['task21_01'] = (task21.main, file_to_args('hw03/in21_01.txt'))
    out['task22_00'] = (task22.main, file_to_args('hw03/in22_00.txt'))
    out['task22_01'] = (task22.main, file_to_args('hw03/in22_01.txt'))
    out['task23_00'] = (task23.main, file_to_args('hw03/in23_00.txt'))
    out['task23_01'] = (task23.main, file_to_args('hw03/in23_01.txt'))
    out['task24_00'] = (task24.main, file_to_args('hw03/in24_00.txt'))
    out['task24_01'] = (task24.main, file_to_args('hw03/in24_01.txt'))
    out['task25_00'] = (task25.main, file_to_args('hw04/in/in25_00.txt'))
    out['task25_01'] = (task25.main, file_to_args('hw04/in/in25_01.txt'))
    out['task25_02'] = (task25.main, file_to_args('hw04/in/in25_02.txt'))
    out['task26_00'] = (task26.main, file_to_args('hw04/in/in26_00.txt'))
    out['task26_01'] = (task26.main, file_to_args('hw04/in/in26_01.txt'))
    out['task27_00'] = (task27.main, file_to_args('hw04/in/in27_00.txt'))
    out['task27_01'] = (task27.main, file_to_args('hw04/in/in27_01.txt'))
    out['task28_00'] = (task28.main, ('hw04/in/blosum62.txt', 'hw04/in/in28_00.txt'))
    out['task28_01'] = (task28.main, ('hw04/in/blosum62.txt', 'hw04/in/in28_01.txt'))
    out['task29_00'] = (task29.main, ('hw04/in/pam250.txt', 'hw04/in/in29_00.txt'))
    out['task29_01'] = (task29.main, ('hw04/in/pam250.txt', 'hw04/in/in29_01.txt'))
    return out


if __name__ == '__main__':
    make(*sys.argv[1:])
