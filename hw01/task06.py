import sys
from collections import Counter


def main(*args):
    fn = args[0][0]
    with open(fn) as fp:
        l = fp.readlines()
        c = Counter(" ".join(l).split())
        print("\n".join(map(lambda t: str(t[0]) + ' ' + str(t[1]), sorted(c.items()))))


if __name__ == '__main__':
    main(sys.argv[1:])
