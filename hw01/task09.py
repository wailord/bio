import sys


def main(*args):
    s = args[0][0]
    s = s.lower()
    d = {'a': 'T', 't': 'A', 'c': 'G', 'g': 'C'}
    for k, v in d.items():
        s = s.replace(k, v)

    print(s[::-1])


if __name__ == '__main__':
    main(sys.argv[1:])
