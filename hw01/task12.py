import re
import sys


def main(*args):
    print(' '.join(map(str, map(lambda it: it.regs[0][0] + 1, re.finditer('(?=' + args[0][1] + ')', args[0][0])))))


if __name__ == '__main__':
    main(sys.argv[1:])
