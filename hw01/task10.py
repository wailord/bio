import sys
from collections import Counter
from operator import itemgetter


def read_ro(g: str):
    return map(lambda it: (it[0], ''.join(it[1:])), map(str.split, filter(len, g.split('>'))))


def calc_GC(tup):
    name, it = tup
    l = len(it)
    c = Counter(it)
    cg = (c['C'] + c['G']) / l
    return name, cg


def main(*args):
    fn = args[0][0]
    with open(fn) as fp:
        l = fp.readlines()
        d = list(read_ro(''.join(l)))
        d = map(calc_GC, d)
        d = sorted(d, key=itemgetter(1))
        boss = d[-1]
        print('{}\n{}'.format(boss[0], boss[1]*100))


if __name__ == '__main__':
    main(sys.argv[1:])
