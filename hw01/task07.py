import sys
from collections import Counter


def main(*args):
    s = args[0][0]
    c = Counter(s)
    print(" ".join(map(lambda t: str(t[1]), sorted(c.items()))))


if __name__ == '__main__':
    main(sys.argv[1:])
