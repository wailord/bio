"""
HumptyDumptysatonawallHumptyDumptyhadagreatfallAlltheKingshorsesandalltheKingsmenCouldntputHumptyDumptyinhisplaceagain
"""
import sys


def main(*args):
    print(args)
    text = args[0][0]
    intervals = list(map(int, args[0][1:]))
    out = []
    for f, t in zip(intervals[0::2], intervals[1::2]):
        out.append(text[f:t + 1])

    print(' '.join(out))


if __name__ == '__main__':
    main(sys.argv[1:])
