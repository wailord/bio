import sys


def main(*args):
    a = int(args[0][0])
    b = int(args[0][1])
    a += 0 if a % 2 else 1
    b = b + 1
    print(sum(range(a, b, 2)))


if __name__ == '__main__':
    main(sys.argv[1:])
