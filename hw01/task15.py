import sys


def read_ro(g: str):
    return map(lambda it: (it[0], ''.join(it[1:])), map(str.split, filter(len, g.split('>'))))


def length_stripper(input, k=2):
    for i in range(len(input) - k + 1):
        yield input[i:i + k]


def fail(s):
    m = len(s)
    f = [0] * m
    j = 0
    for i in range(1, m):
        if s[i] == s[j]:
            f[i] = f[j]
        else:
            f[i] = j
        while j > 0 and s[i] != s[j]:
            j = f[j]
        j = j + 1
    return f


def fail2(P):
    m = len(P)
    f = [0] * m
    j = 0
    for k in range(1, m):
        i = k - 1
        x = f[i]

        while P[x] != P[k]:
            i = x - 1
            if i < 0:
                break
            x = f[i]

        if i < 0:
            f[k] = 0
        else:
            f[k] = f[i] + 1

    return f


def main(*args):
    fn = args[0][0]
    with open(fn) as fp:
        l = fp.readlines()
        m = read_ro(''.join(l))
        _, v = next(m)
        print(v)
        out = fail2(v)

        print(' '.join(map(str, out)))


if __name__ == '__main__':
    main(sys.argv[1:])
