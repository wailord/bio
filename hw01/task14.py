import sys
from itertools import product


def main(*args):
    print('\n'.join(map(lambda it: ''.join(it), product(sorted(args[0][0:-1]), repeat=int(args[0][-1])))))


if __name__ == '__main__':
    main(sys.argv[1:])
