import sys


def main(*args):
    with open(args[0][0]) as file:
        out = []
        lines = file.readlines()
        for l in lines[1::2]:
            out.append(l)
        print("".join(out))


if __name__ == '__main__':
    main(sys.argv[1:])
