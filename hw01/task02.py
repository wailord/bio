import sys


def square(a):
    return a ** 2


def main(*args):
    print(sum(map(square, map(int, args[0]))))


if __name__ == '__main__':
    main(sys.argv[1:])
