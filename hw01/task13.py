import sys
from collections import Counter
from itertools import product


def read_ro(g: str):
    return map(lambda it: (it[0], ''.join(it[1:])), map(str.split, filter(len, g.split('>'))))


def length_stripper(input, k=2):
    for i in range(len(input) - k + 1):
        yield input[i:i + k]


def main(*args):
    fn = args[0][0]
    with open(fn) as fp:
        l = fp.readlines()
        m = read_ro(''.join(l))
        _, v = next(m)
        c = Counter()
        c.update(map(lambda a: str.join('', a), product('ACGT', repeat=4)))
        c.update(length_stripper(v, 4))
        print(' '.join(list(map(lambda it: str(it[1] - 1), sorted(c.items())))))


if __name__ == '__main__':
    main(sys.argv[1:])
