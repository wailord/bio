#!/usr/bin/env python
"""
Tiny python make. For adding new target it is necessary to create target dict and import it in method local_import.
Keys in target dict are names of target, names of targets must be unique over project.
Items in target dict are consist of tuple with function reference and arrays of parameters.
Targets can be organize into hierarchy, by create target with function :func:`make` and giving name of child targets as
list of parameters.
"""
import builtins
import random
import sys
import time
import traceback
from multiprocessing import Process

IMPORTED = False

global rnd
rnd = None
from utils import O


def register_targets(original_function):
    if not hasattr(builtins, 'targets'):
        builtins.targets = dict()
    builtins.targets.update(original_function())
    return original_function


def create_all_target(name: str, d: dict):
    """
    Create target with, which have all targets from dict as subtargets

    :param name: name of new target
    :param d: dict with targets,
    :return: updated dict
    """
    d.update({name: (make, list(d.keys()))})
    return d


def file_to_args(file_name: str):
    out = []
    with open(file_name) as fp:
        for line in fp.readlines():
            out += line.split()
    return out


def print_targets():
    print('\n'.join(
        '{}.{}: ({})'.format(k.__module__, k.__name__, ', '.join(map(str, v))) for k, v in builtins.targets.values()))


class Lazy:

    def lazy_m(self, m, *args, **kwargs):
        point = self.cls(*self.args, **self.kwargs)
        value = getattr(point, m)(*args, **kwargs)
        # self = point
        return value

    def __init__(self, cls, *args, **kwargs):
        self.cls = cls
        self.args = args
        self.kwargs = kwargs
        ml = [func for func in dir(cls) if callable(getattr(cls, func))]
        ml.remove('__class__')
        for m in ml:
            lazy_m = self.lazy_m |O| m

            setattr(self, m, lazy_m)


class StdOut(object):
    def __init__(self, print_to_console=True, write_to_file=None, sys_stdout=None):
        self.print_to_console = print_to_console
        self.write_to_file = write_to_file
        if not sys_stdout:
            self.sys_stdout = sys.stdout
        if write_to_file:
            self.file_output = open(write_to_file, 'w')
        else:
            self.file_output = None

    def write(self, string):
        if self.print_to_console:
            self.sys_stdout.write(string)
        if self.file_output:
            self.file_output.write(string)

    def display(self, string):
        if self.print_to_console:
            self.sys_stdout.write(string + '\n')

    def __delete__(self, instance):
        if self.file_output:
            self.sys_stdout.prin('closing file')
            self.file_output.close()

    def flush(self):
        self.sys_stdout.flush()
        if self.file_output:
            self.file_output.flush()

    def __reduce__(self):
        return StdOut, (self.print_to_console, self.write_to_file)


def _process_package(kv, target, met, *args, **argv):
    """
    Method for calling targets. This method check all exception from target call.

    :param target: target to run (only for informal print)
    :param met: method to run
    :param args: args for method
    """

    global rnd
    if not rnd:
        rnd = random.randint(0, 10000)
        print('#####################################################################')
        print('############## run new task with undefined random ' + str(rnd) + ' ##############')
    else:
        print('##########################################################')
        print('############## defined random ' + str(rnd) + ' ##############')

    if 'stdout' in argv:
        if argv['stdout']:
            sys.stdout = argv['stdout']

    try:
        met(*args, **kv)
    except Exception as error:
        traceback.print_exc()
        m = 'In target {} between call {} \nwith parameters {} \nwas excepted exception: {}'
        print(m.format(target, met, args, error), file=sys.stderr)


def make(*args, parallel=False, debug=False, sleep_between=0, stdout=None):
    """
    Method call all target from args :param:`args` .

    :param args: iterable with name of targets
    :param debug: run make without subprocess, danger for global value, but good for debugging
    :param parallel: run make in parallel way, nowadays without limitation on count of subprocess
    :param sleep_between: sleep time between runs of next target, useful for parallel run with different seed by
    timestamp.
    """
    flags = {'parallel': parallel, 'debug': debug, 'sleep_between': sleep_between, 'stdout': stdout}
    if not hasattr(builtins, 'targets'):
        builtins.targets = dict()
    if not IMPORTED:
        import makefile
        makefile.imports()
    make_target = builtins.targets
    for t in args:
        tar = make_target[t]
        super_kv = {}
        if len(tar) == 2:
            met, par = tar
            kv = {}
        elif len(tar) == 3:
            met, par, kv = tar
        else:
            met, par, kv, super_kv = tar
            flags.update(super_kv)
        parallel = flags['parallel']
        debug = flags['debug']
        sleep_between = flags['sleep_between']
        stdout = flags['stdout']
        if not stdout:
            stdout = StdOut(True)
        par = tuple(par)
        pp = (kv, t, met, *par)
        if debug:
            _process_package(*pp, stdout=stdout)
        else:
            p = Process(target=_process_package, args=pp, kwargs={'stdout': stdout})
            p.start()
            if not parallel:
                p.join()

        time.sleep(sleep_between)


if __name__ == '__main__':
    make(*sys.argv[1:])
