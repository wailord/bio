import sys

from hw04.gap_functions import AffineGap, LinearGap, AbstractGap
from make import register_targets, StdOut
from ndmatrix import NDMatrix
from utils import read_fasta, second, load_matrix_from_file


# you can see, all my code in https://gitlab.com/wailord/bio-secret

def red(s, t, d: NDMatrix, move: NDMatrix, m=None, n=None):
    if not m:
        m = len(s)
    if not n:
        n = len(t)
    s = list(s)
    t = list(t)
    while True:
        nxt = move[m, n]
        if not nxt:
            break
        nm, nn = nxt
        if nm == m - 1 and nn == n - 1:
            pass
        elif nm == m - 1:
            # print('l')
            t.insert(n, '-')
        elif nn == n - 1:
            # print('u')
            s.insert(m, '-')

        m, n = nm, nn

    print(''.join(s))
    print(''.join(t))


def levenshtein_2d(s, t, scoring_matrix: dict = None, gap_function: AbstractGap = None, sub_start=float('inf')):
    if not gap_function:
        gap_function = LinearGap(1)
    d = NDMatrix([len(s) + 1, len(t) + 1])
    move = NDMatrix([len(s) + 1, len(t) + 1])
    m = len(s)
    n = len(t)

    for i in range(1, m + 1):
        d[i, 0] = -gap_function[i]
        move[i, 0] = (i - 1, 0)

    for j in range(1, n + 1):
        d[0, j] = -gap_function[j]
        move[0, j] = (0, j - 1)

    d[0, 0] = 0

    for j in range(1, n + 1):
        for i in range(1, m + 1):
            if scoring_matrix:
                sc = scoring_matrix[s[i - 1], t[j - 1]]
            else:
                sc = 0 if s[i - 1] == t[j - 1] else 1

            # i_gap = gap_length(move, i - 1, j, -1, 0) + 5
            # j_gap = gap_length(move, i, j - 1, 0, -1) + 5
            i_gap = i
            j_gap = j

            price_arr = [*[(d[i-ii, j] - gap_function[ii], (i - 1, j), (i-ii, j)) for ii in range(0, i_gap + 1)],
                         *[(d[i, j-jj] - gap_function[jj], (i, j - 1), (i, j - jj)) for jj in range(0, j_gap + 1)],
                         (d[i - 1, j - 1] + sc, (i - 1, j - 1)),
                         (sub_start, (0, 0))]

            mm = max(price_arr)

            d[i, j] = mm[0]
            move[i, j] = mm[1]

    return d, move


def main(*args):
    sm = load_matrix_from_file(args[0])
    with open(args[1]) as f:
        l = read_fasta('\n'.join(f.readlines()))
        l = [second(it) for it in l]
        m = l[0]
        n = l[1]
        # print(m, n)
        d, move = levenshtein_2d(m, n, sm, AffineGap(11, 1), -float('inf'))
        sys.stdout.display(d.nice_str())
        sys.stdout.display(move.nice_str(fermat='{!s:8}'))
        # print(d.max())
        # print(d.argmax())
        # print(d[d.argmax()])
        print(d[len(m), len(n)])
        red(m, n, d, move)


@register_targets
def get_targets():
    out = {'task30_00': (main, ('hw04/in/blosum62.txt', 'hw04/in/in30_00.txt')),
           'task30_01': (main, ('hw04/in/blosum62.txt', 'hw04/in/in30_01.txt')),
           'task30_02': (main, ('hw04/in/blosum62.txt', 'hw04/in/in30_02.txt')),
           'task30_11': (main, ('hw04/in/blosum62.txt', 'hw04/in/in30_11.txt')),
           'task30_12': (main, ('hw04/in/blosum62.txt', 'hw04/in/in30_12.txt')),
           'task30_13': (
               main, ('hw04/in/blosum62.txt', 'hw04/in/in30_13.txt'), {},
               {'stdout': StdOut(True, 'hw04/in/in30_13.out')}),
           'task30_03': (
               main, ('hw04/in/blosum62.txt', 'hw04/in/in30_03.txt'), {},
               {'stdout': StdOut(True, 'hw04/in/in30_03.out')}),
           'task30_14': (
               main, ('hw04/in/blosum62.txt', 'hw04/in/in30_14.txt'), {},
               {'stdout': StdOut(True, 'hw04/in/in30_14.out')}),
           }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
