import heapq
import sys

from utils import read_fasta


class recursionlimit:
    def __init__(self, limit):
        self.limit = limit
        self.old_limit = sys.getrecursionlimit()

    def __enter__(self):
        sys.setrecursionlimit(self.limit)

    def __exit__(self, type, value, tb):
        sys.setrecursionlimit(self.old_limit)


# you can see, all my code in https://gitlab.com/wailord/bio-secret

def levenshtein_old(s, t, d=None):
    s_oginal, t_original = s, t
    stack = []
    if d is None:
        d = {}

    heapq.heappush(stack, (len(s), len(t), s, t))

    while stack:
        ls, lt, s, t = heapq.heappop(stack)
        if (s, t) in d:
            continue

        if ls == 0:
            d[s, t] = lt
            continue
        if lt == 0:
            d[s, t] = ls
            continue

        if s[ls - 1] == t[lt - 1]:
            cost = 0
        else:
            cost = 1

        one_more_time = False
        if (s[:-1], t) not in d:
            sh = s[:-1]
            th = t
            heapq.heappush(stack, (len(sh), len(th), sh, th))
            one_more_time = True
        if (s, t[:-1]) not in d:
            sh = s
            th = t[:-1]
            heapq.heappush(stack, (len(sh), len(th), sh, th))
            one_more_time = True
        if (s[:-1], t[:-1]) not in d:
            sh = s[:-1]
            th = t[:-1]
            heapq.heappush(stack, (len(sh), len(th), sh, th))
            one_more_time = True

        if one_more_time:
            sh = s
            th = t
            heapq.heappush(stack, (len(sh), len(th), sh, th))
        else:
            sol = min(d[s[:-1], t] + 1,
                      d[s, t[:-1]] + 1,
                      d[s[:-1], t[:-1]] + cost)
            d[s, t] = sol

    return d[s_oginal, t_original]


def levenshtein(s, t):
    m = len(s)
    n = len(t)
    v0 = list(range(n)) + [0]
    v1 = [0] * (n + 1)

    for i in range(m):
        v1[0] = i + 1
        for j in range(n):
            dc = v0[j + 1] + 1
            ic = v1[j] + 1
            sc = v0[j]
            if s[i] != t[j]:
                sc += 1
            v1[j + 1] = min(dc, ic, sc)
        h = v0
        v0 = v1
        v1 = h
    return v0[n]


def main(*args):
    l = list(read_fasta('\n'.join(args)))
    a = l[0][1]
    b = l[1][1]
    # print(a)
    # print(b)
    print(levenshtein(a, b))
    # print(levenshtein_old(a, b))


if __name__ == '__main__':
    main(sys.argv[1:])
