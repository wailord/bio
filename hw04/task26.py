import sys

from ndmatrix import NDMatrix
from utils import read_fasta


# you can see, all my code in https://gitlab.com/wailord/bio-secret


def levenshtein_2d(s, t):
    d = NDMatrix([len(s) + 1, len(t) + 1])
    m = len(s)
    n = len(t)

    for i in range(m + 1):
        d[i, 0] = i

    for j in range(n + 1):
        d[0, j] = j

    for j in range(1, n + 1):
        for i in range(1, m + 1):
            sc = 1
            if s[i - 1] == t[j - 1]:
                sc = 0
            d[i, j] = min(
                d[i - 1, j] + 1,
                d[i, j - 1] + 1,
                d[i - 1, j - 1] + sc
            )

    return d


def red(s, t, d: NDMatrix):
    m = len(s)
    n = len(t)
    s = list(s)
    t = list(t)
    while True:
        p = d[m, n]
        if m == 0 and n == 0:
            break
        if m > 1 and p - 1 == d[m - 1, n]:
            m -= 1
            # print('l')
            t.insert(n, '-')

            continue
        if n > 1 and p - 1 == d[m, n - 1]:
            # print('u')
            n -= 1
            s.insert(m, '-')

            continue
        # print('c')
        m -= 1
        n -= 1

    print(''.join(s))
    print(''.join(t))


def main(*args):
    l = list(read_fasta('\n'.join(args)))
    a = l[0][1]
    b = l[1][1]
    # print(a)
    # print(b)
    d = levenshtein_2d(a, b)
    print(d[len(a), len(b)])
    red(a, b, d)
    # print(levenshtein_old(a, b))


if __name__ == '__main__':
    main(sys.argv[1:])
