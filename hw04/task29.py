import sys

from ndmatrix import NDMatrix
from utils import read_fasta, second, load_matrix_from_file, levenshtein_2d


# you can see, all my code in https://gitlab.com/wailord/bio-secret

def red(s, t, d: NDMatrix, move: NDMatrix):
    m = len(s)
    n = len(t)
    s = list(s)
    t = list(t)
    sn = []
    tn = []
    start = d.argmax()
    while True:
        next = move[start]
        si, sj = start
        ni, nj = next
        if ni <= 0 and ni <= 0:
            break
        if ni < si and nj < sj:
            # print('c')
            sn = [s[ni]] + sn
            tn = [t[nj]] + tn
        elif ni < si:
            # print('l')
            sn = [s[ni]] + sn
        elif nj < sj:
            # print('u')
            tn = [t[nj]] + tn

        start = next
    print(''.join(sn))
    print(''.join(tn))


def main(*args):
    sm = load_matrix_from_file(args[0])
    with open(args[1]) as f:
        l = read_fasta('\n'.join(f.readlines()))
        l = [second(it) for it in l]
        m = l[0]
        n = l[1]
        # print(m, n)
        d, move = levenshtein_2d(m, n, sm, -5, 0)
        # print(d[len(m), len(n)])
        # print(d)
        print(d.max())
        # print(d.argmax())
        # print(d[d.argmax()])
        red(m, n, d, move)


if __name__ == '__main__':
    main(sys.argv[1:])
