from abc import abstractmethod


class AbstractGap:

    @abstractmethod
    def __getitem__(self, item):
        pass


class ConstantGap(AbstractGap):

    def __init__(self, constant):
        self.constant = constant

    def __getitem__(self, item):
        return self.constant


class LinearGap(AbstractGap):

    def __init__(self, constant):
        self.constant = constant

    def __getitem__(self, item):
        return self.constant * item


class AffineGap(AbstractGap):

    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __getitem__(self, item):
        return self.a + self.b * (item - 1)
