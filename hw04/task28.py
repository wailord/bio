import sys

from utils import read_fasta, second, load_matrix_from_file, levenshtein_2d


# you can see, all my code in https://gitlab.com/wailord/bio-secret


def main(*args):
    sm = load_matrix_from_file(args[0])
    with open(args[1]) as f:
        l = read_fasta('\n'.join(f.readlines()))
        l = [second(it) for it in l]
        m = l[0]
        n = l[1]
        # print(m, n)
        d = levenshtein_2d(m, n, sm, -5)
        print(d[len(m), len(n)])


if __name__ == '__main__':
    main(sys.argv[1:])
