import sys

from utils import read_fasta, second


# you can see, all my code in https://gitlab.com/wailord/bio-secret


def lcs(arr):
    s = arr[0]
    sl = len(s)

    res = ""

    for i in range(sl):
        for j in range(i + len(res), sl):
            sij = s[i:j]
            inn = True
            for l in arr[1:]:
                if sij not in l:
                    inn = False
                    break

            if inn and len(sij) >= len(res):
                res = sij

    return res


def main(*args):
    l = list(read_fasta('\n'.join(args)))
    l = [second(it) for it in l]
    print(lcs(l))


if __name__ == '__main__':
    main(sys.argv[1:])
