Just some tips how to use python, and python library in pythonic way.
Wrote in Markdown.
You can find more (later) at https://gitlab.com/wailord/bio
# HW 02
## 11
### Translation dic
```python
{'UUU': 'F', 'CUU': 'L', 'AUU': 'I', 'GUU': 'V',
'UUC': 'F', 'CUC': 'L', 'AUC': 'I', 'GUC': 'V',
'UUA': 'L', 'CUA': 'L', 'AUA': 'I', 'GUA': 'V',
'UUG': 'L', 'CUG': 'L', 'AUG': 'M', 'GUG': 'V',
'UCU': 'S', 'CCU': 'P', 'ACU': 'T', 'GCU': 'A',
'UCC': 'S', 'CCC': 'P', 'ACC': 'T', 'GCC': 'A',
'UCA': 'S', 'CCA': 'P', 'ACA': 'T', 'GCA': 'A',
'UCG': 'S', 'CCG': 'P', 'ACG': 'T', 'GCG': 'A',
'UAU': 'Y', 'CAU': 'H', 'AAU': 'N', 'GAU': 'D',
'UAC': 'Y', 'CAC': 'H', 'AAC': 'N', 'GAC': 'D',
'UAA': '', 'CAA': 'Q', 'AAA': 'K', 'GAA': 'E',
'UAG': '', 'CAG': 'Q', 'AAG': 'K', 'GAG': 'E',
'UGU': 'C', 'CGU': 'R', 'AGU': 'S', 'GGU': 'G',
'UGC': 'C', 'CGC': 'R', 'AGC': 'S', 'GGC': 'G',
'UGA': '', 'CGA': 'R', 'AGA': 'R', 'GGA': 'G',
'UGG': 'W', 'CGG': 'R', 'AGG': 'R', 'GGG': 'G', }
```
### Iteration through list
```python
l = 'from:to:step' 
print(l[0::3])
```
Out:
`fmot`
### Zzzzzzip
```python
a = '12345'
b = 'abcde'
c = ['I', 'am', 'an', 'airplane', '.']
z = zip(a,b,c)
# you can iterate throught zip object, just for printing make list
print(list(z))
```
### How to easy print solutions
```python
print(''.join(map(str, your_list)))
```

## 12
### Black magic called re
I hopoe, that you like regular expression.
You can use library called re.

## 13
Be lazy, use library.
### Itertools
```python
from itertools import product
res = product('DNA', repeat=3)
print('\n'.join(map(lambda t: ''.join(t), res)))
```
### Collections
Maybe counter? :)

