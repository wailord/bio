import sys

from make import register_targets, StdOut

_DEBUG = False


# you can see, all my code in https://gitlab.com/wailord/bio-secret


def forward(states, observation, tp, ep, inn, backward=False):
    if _DEBUG:
        print(inn)
        print(observation)
        print(states)
        print(tp)
        print(ep)

    alpha = dict()
    f = inn[0]
    for s in states:
        alpha[s, 0] = ep[s, f] * 1 / len(states)

    if backward:
        for s in states:
            alpha[s, 0] = 1

    for t, o in enumerate(inn[1:]):
        for i in states:
            prev = [alpha[j, t] * tp[j, i] for j in states]
            prev = sum(prev) * ep[i, o]
            alpha[i, t + 1] = prev

    return alpha


def backward(states, observation, tp, ep, inn):
    if _DEBUG:
        print(inn)
        print(observation)
        print(states)
        print(tp)
        print(ep)

    alpha = dict()
    f = inn[0]
    for s in states:
        alpha[s, 0] = ep[s, f] * 1 / len(states)

    if backward:
        for s in states:
            alpha[s, len(inn)] = 1

    for t, o in zip(range(len(inn) - 1, 0, -1), reversed(inn)):
        for i in states:
            prev = [alpha[j, t + 1] * tp[i, j] * ep[j, o] for j in states]
            prev = sum(prev)
            alpha[i, t] = prev

    return alpha


def gama(alpha, beta, states, inn):
    out = dict()
    for t in range(len(inn)):
        for i in states:
            out[i, t] = alpha[i, t] * beta[i, t + 1] / (sum([alpha[j, t] * beta[j, t + 1] for j in states]))

    return out


def fi(tp, ep, alpha, beta, states, inn):
    out = dict()
    for i in states:
        for j in states:
            for t, y in enumerate(inn[1:]):
                h = alpha[i, t] * tp[i, j] * beta[j, t + 1 + 1] * ep[j, y]
                s = 0
                for ii in states:
                    for jj in states:
                        s += alpha[ii, t] * tp[ii, jj] * beta[jj, t + 1 + 1] * ep[jj, y]
                out[i, j, t] = h / s

    return out


def astar(gamma, fi, states, inn):
    out = dict()
    for i in states:
        for j in states:
            out[i, j] = sum([fi[i, j, t] for t in range(len(inn) - 1)]) / sum(
                [gamma[i, t] for t in range(len(inn) - 1)])
    return out


def bstar(gamma, states, inn):
    out = dict()
    for i in states:
        for t, vk in enumerate(inn):
            h = sum([gamma[i, t] * (1 if y == vk else 0) for t, y in enumerate(inn)])
            s = sum([gamma[i, t] for t, y in enumerate(inn)])
            out[i, vk] = h / s

    return out


def main(f):
    with open(f) as fp:
        iterrations = int(fp.readline())
        fp.readline()
        inn = fp.readline()[:-1]
        fp.readline()
        observation = fp.readline().split()
        fp.readline()
        states = fp.readline().split()
        fp.readline()
        fp.readline()
        tp = dict()
        ep = dict()
        for f in states:
            tp.update({(f, t): float(p) for t, p in zip(states, fp.readline().split()[1:])})
        fp.readline()
        fp.readline()
        for f in states:
            ep.update({(f, t): float(p) for t, p in zip(observation, fp.readline().split()[1:])})

        for itt in range(iterrations):
            f = forward(states, observation, tp, ep, inn)
            b = backward(states, observation, tp, ep, inn)
            g = gama(f, b, states, inn)
            ff = fi(tp, ep, f, b, states, inn)
            ttp = astar(g, ff, states, inn)
            ep = bstar(g, states, inn)
            tp = ttp

        eps = 0.0001
        print('\t' + '\t'.join(states))
        for f in states:
            l = f + '\t'
            s = sum([tp[f, t] for t in states])
            if s:
                l += '\t'.join(['{:.3f}'.format(eps + tp[f, t] / s) for t in states])
            else:
                l += '\t'.join(['{:.3f}'.format(eps + 1 / len(states)) for t in states])
            print(l)
        print('--------')
        print('\t' + '\t'.join(observation))
        for f in states:
            l = f + '\t'
            s = sum([ep[f, t] for t in observation])
            if s:
                l += '\t'.join(['{:.3f}'.format(eps + ep[f, t] / s) for t in observation])
            else:
                l += '\t'.join(['{:.3f}'.format(eps + + 1 / len(states)) for t in observation])
            print(l)

            # print('--------------------------------------------------')


@register_targets
def get_targets():
    out = {
        'task39_00': (main, ('hw05/in/in39_00.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_00.out')}),
        'task39_01': (main, ('hw05/in/in39_01.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_01.out')}),
        'task39_02': (main, ('hw05/in/in39_02.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_02.out')}),
    }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
