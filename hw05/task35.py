import sys

from make import register_targets, StdOut

_DEBUG = False
_OLD = False


# you can see, all my code in https://gitlab.com/wailord/bio-secret

def forward(states, observation, tp, ep, inn):
    if _DEBUG:
        print(inn)
        print(observation)
        print(states)
        print(tp)
        print(ep)

    alpha = dict()
    f = inn[0]
    for s in states:
        alpha[s, 0] = ep[s, f] * 1 / len(states)

    for t, o in enumerate(inn[1:]):
        for i in states:
            prev = [alpha[j, t] * tp[j, i] for j in states]
            prev = sum(prev) * ep[i, o]
            alpha[i, t + 1] = prev

    return alpha


def main(f):
    with open(f) as fp:
        inn = fp.readline()[:-1]
        fp.readline()
        observation = fp.readline().split()
        fp.readline()
        states = fp.readline().split()
        fp.readline()
        fp.readline()
        tp = dict()
        ep = dict()
        for f in states:
            tp.update({(f, t): float(p) for t, p in zip(states, fp.readline().split()[1:])})
        fp.readline()
        fp.readline()
        for f in states:
            ep.update({(f, t): float(p) for t, p in zip(observation, fp.readline().split()[1:])})

        a = forward(states, observation, tp, ep, inn)
        prev = [a[k, len(inn)-1] for k in states]
        print(sum(prev))

        if _DEBUG:
            print(inn)
            print(observation)
            print(states)
            print(tp)
            print(ep)

        if _OLD:
            # tp transition probability
            # ep emission probability
            # in sequence
            t1 = dict()
            for s in states:
                t1[s, 0] = 1 / len(states)

            for i, o in enumerate(inn, 1):
                for l in states:
                    prev = [t1[k, i - 1] * tp[k, l] for k in states]
                    t1[l, i] = sum(prev) * ep[l, o]

            prev = [t1[k, len(inn)] for k in states]
            print(sum(prev))


@register_targets
def get_targets():
    out = {
        'task35_00': (main, ('hw05/in/in35_00.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_00.out')}),
        'task35_01': (main, ('hw05/in/in35_01.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_01.out')}),
        'task35_02': (main, ('hw05/in/in35_02.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_02.out')}),
    }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
