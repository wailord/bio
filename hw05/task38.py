import itertools
import sys
from collections import Counter

from make import register_targets, StdOut

_DEBUG = False


# you can see, all my code in https://gitlab.com/wailord/bio-secret


def viterbi_path(states, observation, tp, ep, inn):
    if _DEBUG:
        print(inn)
        print(observation)
        print(states)
        print(tp)
        print(ep)

    t1 = dict()
    t2 = dict()
    f = inn[0]
    for s in states:
        t1[s, 0] = ep[s, f] * 1 / len(states)
        t2[s, 0] = 0

    for i, o in enumerate(inn[1:], 1):
        for j in states:
            prev = [(t1[k, i - 1] * tp[k, j] * ep[j, o], k) for k in states]
            prev = sorted(prev, reverse=True)
            t1[j, i] = prev[0][0]
            t2[j, i] = prev[0][1]

    prev = [(t1[k, len(inn) - 1], k) for k in states]
    prev = sorted(prev, reverse=True)
    Zt = prev[0][1]
    out = []

    if _DEBUG:
        print(t1)
        print(t2)

    for i in range(len(inn) - 1, 0, -1):
        out += [Zt]
        Zt = t2[Zt, i]

    out += [Zt]
    out = reversed(out)
    out = ''.join(out)
    return out


def estimate(states, observation, path, inn):
    ep = Counter(zip(path, inn))
    tp = Counter(zip(path[:-1], path[1:]))
    tp += Counter({tuple(it): 0 for it in itertools.product(states, repeat=2)})

    eout = dict()
    tout = dict()
    for f in states:
        s = sum([tp[f, t] for t in states])
        if s:
            tout.update({(f, t): tp[f, t] / s for t in states})
        else:
            tout.update({(f, t): 0.0001 + 1 / len(states) for t in states})
    for f in states:
        s = sum([ep[f, t] for t in observation])
        if s:
            eout.update({(f, t): ep[f, t] / s for t in observation})
        else:
            eout.update({(f, t): 1 / len(states) for t in observation})
    return tout, eout


def main(f):
    with open(f) as fp:
        iterrations = int(fp.readline())
        fp.readline()
        inn = fp.readline()[:-1]
        fp.readline()
        observation = fp.readline().split()
        fp.readline()
        states = fp.readline().split()
        fp.readline()
        fp.readline()
        tp = dict()
        ep = dict()
        for f in states:
            tp.update({(f, t): float(p) for t, p in zip(states, fp.readline().split()[1:])})
        fp.readline()
        fp.readline()
        for f in states:
            ep.update({(f, t): float(p) for t, p in zip(observation, fp.readline().split()[1:])})

        if _DEBUG:
            print(inn)
            print(observation)
            print(states)
            print(tp)
            print(ep)

        for i in range(iterrations):
            p = viterbi_path(states, observation, tp, ep, inn)
            tp, ep = estimate(states, observation, p, inn)

        print('\t' + '\t'.join(states))
        for f in states:
            l = f + '\t'
            s = sum([tp[f, t] for t in states])
            if s:
                l += '\t'.join(['{:.3f}'.format(0.0001 + tp[f, t] / s) for t in states])
            else:
                l += '\t'.join(['{:.3f}'.format(0.0001 + 1 / len(states)) for t in states])
            print(l)
        print('--------')
        print('\t' + '\t'.join(observation))
        for f in states:
            l = f + '\t'
            s = sum([ep[f, t] for t in observation])
            if s:
                l += '\t'.join(['{:.3f}'.format(0.0001 + ep[f, t] / s) for t in observation])
            else:
                l += '\t'.join(['{:.3f}'.format(0.0001 + 1 / len(states)) for t in observation])
            print(l)


@register_targets
def get_targets():
    out = {
        'task38_00': (main, ('hw05/in/in38_00.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_00.out')}),
        'task38_01': (main, ('hw05/in/in38_01.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_01.out')}),
        'task38_02': (main, ('hw05/in/in38_02.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_02.out')}),
    }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
