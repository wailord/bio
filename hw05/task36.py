import itertools
import sys
from collections import Counter

from make import register_targets, StdOut

_DEBUG = True


# you can see, all my code in https://gitlab.com/wailord/bio-secret

def main(f):
    with open(f) as fp:
        inn = fp.readline()[:-1]
        fp.readline()
        observation = fp.readline().split()
        fp.readline()
        path = fp.readline()[:-1]
        fp.readline()
        states = fp.readline().split()

        ep = Counter(zip(path, inn))
        tp = Counter(zip(path[:-1], path[1:]))
        tp += Counter({tuple(it): 0 for it in itertools.product(states, repeat=2)})

        print('\t'+'\t'.join(states))
        for f in states:
            l = f + '\t'
            s = sum([tp[f, t] for t in states])
            if s:
                l += '\t'.join(['{:.3f}'.format(0.0001 + tp[f, t]/s) for t in states])
            else:
                l += '\t'.join(['{:.3f}'.format(0.0001 + 1/len(states)) for t in states])
            print(l)
        print('--------')
        print('\t' + '\t'.join(observation))
        for f in states:
            l = f + '\t'
            s = sum([ep[f, t] for t in observation])
            if s:
                l += '\t'.join(['{:.3f}'.format(0.0001 + ep[f, t] / s) for t in observation])
            else:
                l += '\t'.join(['{:.3f}'.format(0.0001 + 1 / len(states)) for t in observation])
            print(l)


@register_targets
def get_targets():
    out = {
        'task36_00': (main, ('hw05/in/in36_00.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_00.out')}),
        'task36_01': (main, ('hw05/in/in36_01.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_01.out')}),
        'task36_02': (main, ('hw05/in/in36_02.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_02.out')}),
    }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
