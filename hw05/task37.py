import sys

from make import register_targets, StdOut
from utils import first

_DEBUG = False


def viterbi_path(states, observation, tp, ep, inn):
    if _DEBUG:
        print(inn)
        print(observation)
        print(states)
        print(tp)
        print(ep)

    t1 = dict()
    t2 = dict()
    f = inn[0]
    for s in states:
        t1[s, 0] = ep[s, f] * 1
        t2[s, 0] = 0

    for i, o in enumerate(inn[1:], 1):
        for j in states:
            prev = [(t1[k, i - 1] * tp[k, j] * ep[j, o], k) for k in states]
            prev = sorted(prev, reverse=True)
            t1[j, i] = sum(map(first, prev))
            t2[j, i] = prev[0][1]

    prev = [(t1[k, len(inn) - 1], k) for k in states]
    prev = sorted(prev, reverse=True)
    Zt = prev[0][1]
    out = []

    if _DEBUG:
        print(t1)
        print(t2)

    for i in range(len(inn) - 1, 0, -1):
        out += [Zt]
        Zt = t2[Zt, i]

    out += [Zt]
    out = reversed(out)
    out = ''.join(out)
    return out


def forward(states, observation, tp, ep, inn, backward=False):
    if _DEBUG:
        print(inn)
        print(observation)
        print(states)
        print(tp)
        print(ep)

    alpha = dict()
    f = inn[0]
    for s in states:
        alpha[s, 0] = ep[s, f] * 1 / len(states)

    if backward:
        for s in states:
            alpha[s, 0] = 1

    for t, o in enumerate(inn[1:]):
        for i in states:
            prev = [alpha[j, t] * tp[j, i] for j in states]
            prev = sum(prev) * ep[i, o]
            alpha[i, t + 1] = prev

    return alpha


def backward(states, observation, tp, ep, inn):
    if _DEBUG:
        print(inn)
        print(observation)
        print(states)
        print(tp)
        print(ep)

    alpha = dict()
    f = inn[0]
    for s in states:
        alpha[s, 0] = ep[s, f] * 1 / len(states)

    if backward:
        for s in states:
            alpha[s, len(inn)] = 1

    for t, o in zip(range(len(inn) - 1, 0, -1), reversed(inn)):
        for i in states:
            prev = [alpha[j, t + 1] * tp[i, j] * ep[j, o] for j in states]
            prev = sum(prev)
            alpha[i, t] = prev

    return alpha


# you can see, all my code in https://gitlab.com/wailord/bio-secret

def main(f):
    with open(f) as fp:
        inn = fp.readline()[:-1]
        fp.readline()
        observation = fp.readline().split()
        fp.readline()
        states = fp.readline().split()
        fp.readline()
        fp.readline()
        tp = dict()
        ep = dict()
        for f in states:
            tp.update({(f, t): float(p) for t, p in zip(states, fp.readline().split()[1:])})
        fp.readline()
        fp.readline()
        for f in states:
            ep.update({(f, t): float(p) for t, p in zip(observation, fp.readline().split()[1:])})

        if _DEBUG:
            print(inn)
            print(observation)
            print(states)
            print(tp)
            print(ep)

        f = forward(states, observation, tp, ep, inn)
        b = backward(states, observation, tp, ep, inn)

        print('\t'.join(states))
        for i in range(len(inn)):            # fl = [f[s, i] for s in states]
            # bl = [b[s, i + 1] for s in states]
            # fs = sum(fl)
            # bs = sum(bl)
            # fl = [it / fs for it in fl]
            # bl = [it / bs for it in bl]
            # l = [i * j for i, j in zip(fl, bl)]
            # ls = sum(l)
            # l = [it / ls for it in l]
            # print('\t'.join(map('{:.4f}'.format, l)))
            # # print([(f[s, i], b[s, 1 + i]) for s in states])

            l = [f[s, i] * b[s, 1 + i] for s in states]
            ls = sum(l)
            l = [it / ls for it in l]
            print('\t'.join(map('{:.4f}'.format, l)))

        t1 = dict()
        # t2 = dict()
        f = inn[0]
        for s in states:
            t1[s, 0] = 1 / len(states)
            # t2[s, 0] = 0

        # for i, o in enumerate(inn, 1):
        #     for j in states:
        #         prev = [t1[k, i - 1] * tp[k, j] for k in states]
        #         t1[j, i] = sum(prev)
        #         print(j, prev, t1[j, i])
        #         # prev = sorted(prev, reverse=True)
        #         # print(j, t1[j, i], prev)
        #         # t2[j, i] = prev[0][1]

        # prev = [(t1[k, len(inn)], k) for k in states]
        # prev = sorted(prev, reverse=True)
        # # Zt = prev[0][1]
        # # out = []
        # print(t1)
        # print(prev)
        # print(sum(map(first, prev)))


@register_targets
def get_targets():
    out = {
        'task37_00': (main, ('hw05/in/in37_00.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_00.out')}),
        'task37_01': (main, ('hw05/in/in37_01.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_01.out')}),
        'task37_02': (main, ('hw05/in/in37_02.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_02.out')}),
    }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
