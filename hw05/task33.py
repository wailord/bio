import sys

from make import register_targets, StdOut


# you can see, all my code in https://gitlab.com/wailord/bio-secret

def main(f):
    with open(f) as fp:
        path = fp.readline()[:-1]
        fp.readline()
        states = fp.readline().split()
        fp.readline()
        out = fp.readline()[:-1]
        fp.readline()
        out_states = fp.readline().split()
        fp.readline()
        fp.readline()
        tp = dict()
        for f in out_states:
            tp.update({(t, f): float(p) for t, p in zip(states, fp.readline().split()[1:])})
        # print(tp)
        # prob = {s: 1/len(states) for s in states}
        p = 1
        for f, t in zip(path, out):
            # print(f,t)
            p *= tp[(f, t)]
        print(p)


@register_targets
def get_targets():
    out = {
        'task33_00': (main, ('hw05/in/in33_00.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in32_00.out')}),
        'task33_01': (main, ('hw05/in/in33_01.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in32_01.out')}),
    }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
