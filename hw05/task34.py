import sys

from make import register_targets, StdOut

_DEBUG = False

# you can see, all my code in https://gitlab.com/wailord/bio-secret

def main(f):
    with open(f) as fp:
        inn = fp.readline()[:-1]
        fp.readline()
        observation = fp.readline().split()
        fp.readline()
        states = fp.readline().split()
        fp.readline()
        fp.readline()
        tp = dict()
        op = dict()
        for f in states:
            tp.update({(t, f): float(p) for t, p in zip(states, fp.readline().split()[1:])})
        fp.readline()
        fp.readline()
        for f in states:
            op.update({(f, t): float(p) for t, p in zip(observation, fp.readline().split()[1:])})

        if _DEBUG:
            print(inn)
            print(observation)
            print(states)
            print(tp)
            print(op)

        t1 = dict()
        t2 = dict()
        f = inn[0]
        for s in states:
            t1[s, 0] = op[s, f] * 1 / len(states)
            t2[s, 0] = 0

        for i, o in enumerate(inn[1:], 1):
            for j in states:
                prev = [(t1[k, i - 1] * tp[k, j] * op[j, o], k) for k in states]
                prev = sorted(prev, reverse=True)
                t1[j, i] = prev[0][0]
                t2[j, i] = prev[0][1]

        prev = [(t1[k, len(inn) - 1], k) for k in states]
        prev = sorted(prev, reverse=True)
        Zt = prev[0][1]
        out = []

        if _DEBUG:
            print(t1)
            print(t2)

        for i in range(len(inn)-1, 0, -1):
            out += [Zt]
            Zt = t2[Zt, i]

        out += [Zt]
        out = reversed(out)
        out = ''.join(out)
        print(out)


@register_targets
def get_targets():
    out = {
        'task34_00': (main, ('hw05/in/in34_00.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_00.out')}),
        'task34_01': (main, ('hw05/in/in34_01.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_01.out')}),
        'task34_02': (main, ('hw05/in/in34_02.txt',), {}, {'stdout': StdOut(True, 'hw05/in/in34_02.out')}),
    }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
