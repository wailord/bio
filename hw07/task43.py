from make import register_targets, StdOut
from utils import first, print_2d_dict

_DEBUG = False

# you can see, all my code in https://gitlab.com/wailord/bio-secret

import sys

sys.setrecursionlimit(10000)


def read_dm(fp):
    n = int(fp.readline())
    matrix = {}
    for i in range(n):
        matrix.update({(i, j): v for j, v in enumerate(map(int, fp.readline().split()))})

    return n, matrix


def Q(matrix) -> dict:
    nodes = get_nodes(matrix)
    n = len(nodes)
    q = {}
    for i in nodes:
        for j in nodes:
            if i != j:
                q[i, j] = (n - 2) * matrix[i, j] - sum(matrix[i, k] for k in nodes) - sum(matrix[j, k] for k in nodes)
    return q


def get_nodes(matrix):
    nodes = set(map(first, matrix.keys()))
    return nodes


def main(f):
    with open(f) as fp:
        nodes_init, matrix = read_dm(fp)
        out = []
        u = nodes_init
        while True:
            q = Q(matrix)
            nodes = get_nodes(matrix)
            if len(nodes) == 2:
                m = min(map(lambda it: (it[1], it[0]), q.items()))
                if _DEBUG:
                    print('2', m, nodes)
                f, g = m[1]
                out.append((f, g, -m[0]/2))
                break

            if _DEBUG:
                print_2d_dict(q)
            m = min(map(lambda it: (it[1], it[0]), q.items()))
            if _DEBUG:
                print(m, nodes)
            f, g = m[1]
            p = 0.5 * matrix[f, g] + 1 / (2 * (len(nodes) - 2)) * \
                (sum(matrix[f, k] for k in nodes) - sum(matrix[g, k] for k in nodes))
            out.append((f, u, p))
            out.append((g, u, matrix[f, g] - p))
            if _DEBUG:
                print(out)
            matrix[u, u] = 0
            for k in nodes:
                p = 0.5 * (matrix[f, k] + matrix[g, k] - matrix[f, g])
                matrix[k, u] = p
                matrix[u, k] = p
            for k in get_nodes(matrix):
                if (k, f) in matrix:
                    del matrix[k, f]
                if (f, k) in matrix:
                    del matrix[f, k]
                if (k, g) in matrix:
                    del matrix[k, g]
                if (g, k) in matrix:
                    del matrix[g, k]

            u += 1

        out.extend([(it[1], it[0], it[2]) for it in out])
        print('\n'.join('{}->{}:{:.3f}'.format(*it) for it in sorted(out)))


@register_targets
def get_targets():
    out = {
        'task43_00': (main, ('hw07/in/in43_00.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in41_00.out')}),
        'task43_01': (main, ('hw07/in/in43_01.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in41_01.out')}),
        'task43_02': (main, ('hw07/in/in43_02.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in41_02.out')}),
        'task43_03': (main, ('hw07/in/in43_03.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in41_02.out')}),
        'task43_04': (main, ('hw07/in/in43_04.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in41_02.out')}),
    }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
