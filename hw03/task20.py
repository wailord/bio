import sys

from utils import read_fasta, second, all_element_identical


# you can see, all my code in https://gitlab.com/wailord/bio-secret


def main(*args):
    a = args[0]
    b = args[1]
    print(len(a) - sum(map(all_element_identical, zip(a, b))))


if __name__ == '__main__':
    main(sys.argv[1:])
