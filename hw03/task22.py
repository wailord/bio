import sys

import numpy as np
from utils import read_fasta, second

# you can see, all my code in https://gitlab.com/wailord/bio-secret

def profile_matrix(arr):
    lst = list(sorted(np.unique(arr)))
    out = np.zeros([len(lst), arr.shape[1]], dtype=int)
    for i, elm in enumerate(lst):
        out[i, :] = (arr == elm).sum(axis=0)
    return lst, out


def main(*args):
    l = read_fasta('\n'.join(args))
    l = list(map(second, l))
    l = [list(it) for it in l]
    indices, pm = profile_matrix(np.asanyarray(l))
    print(''.join(indices[i] for i in pm.argmax(axis=0)))
    for i, elm in enumerate(indices):
        print(elm + ':', *pm[i, :])


if __name__ == '__main__':
    main(sys.argv[1:])
