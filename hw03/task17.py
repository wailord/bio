import re
import sys


def convert(l):
    d = {
        'UUU': 'F', 'CUU': 'L', 'AUU': 'I', 'GUU': 'V',
        'UUC': 'F', 'CUC': 'L', 'AUC': 'I', 'GUC': 'V',
        'UUA': 'L', 'CUA': 'L', 'AUA': 'I', 'GUA': 'V',
        'UUG': 'L', 'CUG': 'L', 'AUG': 'M', 'GUG': 'V',
        'UCU': 'S', 'CCU': 'P', 'ACU': 'T', 'GCU': 'A',
        'UCC': 'S', 'CCC': 'P', 'ACC': 'T', 'GCC': 'A',
        'UCA': 'S', 'CCA': 'P', 'ACA': 'T', 'GCA': 'A',
        'UCG': 'S', 'CCG': 'P', 'ACG': 'T', 'GCG': 'A',
        'UAU': 'Y', 'CAU': 'H', 'AAU': 'N', 'GAU': 'D',
        'UAC': 'Y', 'CAC': 'H', 'AAC': 'N', 'GAC': 'D',
        'UAA': '', 'CAA': 'Q', 'AAA': 'K', 'GAA': 'E',
        'UAG': '', 'CAG': 'Q', 'AAG': 'K', 'GAG': 'E',
        'UGU': 'C', 'CGU': 'R', 'AGU': 'S', 'GGU': 'G',
        'UGC': 'C', 'CGC': 'R', 'AGC': 'S', 'GGC': 'G',
        'UGA': '', 'CGA': 'R', 'AGA': 'R', 'GGA': 'G',
        'UGG': 'W', 'CGG': 'R', 'AGG': 'R', 'GGG': 'G', }
    out = []
    for xyz in zip(l[0::3], l[1::3], l[2::3]):
        x, y, z = xyz
        xyz = x + y + z
        if xyz in d:
            out.append(d[xyz])
    return ''.join(out)


def to_rna(s):
    return s.replace('T', 'U')


def reverse_complement(s: str):
    s = s.lower()
    d = {'a': 'T', 't': 'A', 'c': 'G', 'g': 'C'}
    for k, v in d.items():
        s = s.replace(k, v)

    return ''.join(s[::-1])


def get_longest_orf(s):
    s_rna = to_rna(s)
    q = re.compile('(AUG(...)*?(UAA|UAG|UGA))')
    m = q.findall(s_rna)
    out = ''
    mmax = 0
    for r, _, _ in m:
        if mmax < len(r):
            out = r
            mmax = len(r)
    return out


def main(*args):
    s = args[0]
    out = get_longest_orf(s)

    s = reverse_complement(s)
    out1 = get_longest_orf(s)
    if len(out) > len(out1):
        print(convert(out))
    else:
        print(convert(out1))


if __name__ == '__main__':
    main(sys.argv[1:])
