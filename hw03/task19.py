import sys

from utils import read_fasta, second, all_element_identical


# you can see, all my code in https://gitlab.com/wailord/bio-secret


def main(*args):
    s = '\n'.join(args)

    l = list(map(second, read_fasta(s)))
    le = list(enumerate(l))
    d = dict()

    for i, it_i in le:
        for j, it_j in le:
            d[i, j] = 1 - (sum(map(all_element_identical, zip(it_i, it_j))) / len(it_i))


    for i in range(len(l)):
        print(' '.join(["{:.5f}".format(d[i, j]) for j in range(len(l))]))


if __name__ == '__main__':
    main(sys.argv[1:])
