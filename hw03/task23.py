import sys

from utils import read_fasta, reverse_complement


# you can see, all my code in https://gitlab.com/wailord/bio-secret


def main(*args):
    l = list(read_fasta('\n'.join(args)))
    d = l[0][1]
    for i in range(len(d)):
        for j in range(4, 13):
            dd = d[i:i + j]
            if len(dd) == j:
                if dd == reverse_complement(dd):
                    print(i + 1, j)


if __name__ == '__main__':
    main(sys.argv[1:])
