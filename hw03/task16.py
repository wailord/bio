import sys
from collections import Counter
from math import log10


def main(*args):
    s = args[0][0]
    arr = map(float, args[0][1:])
    cs = Counter(s)
    out = []

    for prob in arr:
        gc = log10(prob / 2)
        at = log10((1 - prob) / 2)
        out += [gc * (cs['G'] + cs['C']) + at * (cs['A'] + cs['T'])]

    print(' '.join(map("{:.3f}".format, out)))


if __name__ == '__main__':
    main(sys.argv[1:])
