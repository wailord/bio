import sys

from utils import read_fasta, second, all_element_identical


# you can see, all my code in https://gitlab.com/wailord/bio-secret

def transition(a, b):
    a, b = sorted([a, b])
    return (a == 'A' and b == 'G') or (a == 'C' and b == 'T')


def transversion(a, b):
    return not transition(a, b) and a != b


def main(*args):
    l = read_fasta('\n'.join(args))
    l = list(map(second, l))
    a = l[0]
    b = l[1]

    trs = sum(map(lambda it: transition(it[0], it[1]), zip(a, b)))
    trv = sum(map(lambda it: transversion(it[0], it[1]), zip(a, b)))
    print(trs/trv)


if __name__ == '__main__':
    main(sys.argv[1:])
