import sys

from utils import read_fasta, get_all_orf, reverse_complement, convert


def main(*args):
    s = '\n'.join(args)
    n, d = next(read_fasta(s))
    print(d)
    # n, d = args
    out = list(get_all_orf(d))
    d = reverse_complement(d)
    out += get_all_orf(d)
    print('\n'.join(set(map(convert, out))))


if __name__ == '__main__':
    main(sys.argv[1:])
