import sys

from utils import read_fasta, convert, to_rna


# you can see, all my code in https://gitlab.com/wailord/bio-secret


def main(*args):
    l = list(read_fasta('\n'.join(args)))
    d = l[0][1]
    for _, s in l[1:]:
        d = d.replace(s, '')

    print(convert(to_rna(d)))


if __name__ == '__main__':
    main(sys.argv[1:])
