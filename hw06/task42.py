from make import register_targets, StdOut
from utils import reverse_complement

_DEBUG = False

# you can see, all my code in https://gitlab.com/wailord/bio-secret

import sys

sys.setrecursionlimit(10000)


def main(f):
    with open(f) as fp:
        kmers = fp.readlines()
        kmers = [k.replace('\n', '') for k in kmers]
        nodes, paths = de_bruijn(kmers)
        print('\n'.join(map(str, paths)))
        if 'CA' in nodes:
            nodes = list(nodes)
            nodes = ['CA'] + nodes
        out = find_circle(nodes, paths)
        if out:
            for o in out:
                print('aaa')
                cycle = get_cycle(o)
                print(cycle)
        else:
            print('false')


def get_cycle(o):
    o_fist_last = o.index(o[-1])
    o = o[o_fist_last:-1]
    out = o[0]
    out += ''.join([oo[-1] for oo in o[1:]])
    out = out[:-len(o[0]) + 1]
    return out


def dfs(node, paths, known, top):
    known.add(node)
    if node in top:
        top.append(node)
        return [top[:]]

    out = []
    top.append(node)
    for f, t in paths:
        if f == node:
            o = dfs(t, paths, known, top)
            out.extend(o)

    top.pop()
    return out


def find_circle(nodes, paths):
    known = set()
    out = []
    for node in nodes:
        if node not in known:
            print(f'init with dfs {node}')
            kn = set()
            top = []
            o = dfs(node, paths, kn, top)
            if o:
                out.extend(o)
            known.union(kn)

    return out


def de_bruijn(kmers):
    if _DEBUG:
        print('rev:')
        print('\n'.join(kmers))
    for k in kmers:
        rev = reverse_complement(k)
        if rev not in kmers:
            kmers.append(rev)
    pref = [k[:-1] for k in kmers]
    suff = [k[1:] for k in kmers]
    nodes = set(pref)
    nodes = nodes.union(set(suff))
    paths = []
    for i in range(len(pref)):
        paths.append((pref[i], suff[i]))
    if _DEBUG:
        print('kmers:')
        print('\n'.join(sorted(kmers)))
    paths = sorted(set(paths))
    if _DEBUG:
        print('\n'.join(map(lambda it: '({}, {})'.format(it[0], it[1]), paths)))

    return nodes, paths


@register_targets
def get_targets():
    out = {
        'task42_00': (main, ('hw06/in/in42_00.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in41_00.out')}),
        'task42_01': (main, ('hw06/in/in42_01.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in41_01.out')}),
        'task42_02': (main, ('hw06/in/in42_02.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in41_02.out')}),
    }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
