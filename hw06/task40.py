import sys

from make import register_targets, StdOut
from utils import reverse_complement

_DEBUG = False


# you can see, all my code in https://gitlab.com/wailord/bio-secret

def main(f):
    with open(f) as fp:
        kmers = fp.readlines()
        kmers = [k.replace('\n', '') for k in kmers]
        if _DEBUG:
            print('rev:')
            print('\n'.join(kmers))

        for k in kmers:
            rev = reverse_complement(k)
            if rev not in kmers:
                kmers.append(rev)

        pref = [k[:-1] for k in kmers]
        suff = [k[1:] for k in kmers]

        out = []
        for pi, p in enumerate(pref):
            for si, s in enumerate(suff):
                if pi == si:
                    out.append((p, s))

        if _DEBUG:
            print('kmers:')
            print('\n'.join(sorted(kmers)))

        out = sorted(set(out))
        print('\n'.join(map(lambda it: '({}, {})'.format(it[0], it[1]), out)))


@register_targets
def get_targets():
    out = {
        'task40_00': (main, ('hw06/in/in40_00.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in40_00.out')}),
        'task40_01': (main, ('hw06/in/in40_01.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in40_01.out')}),
        'task40_02': (main, ('hw06/in/in40_02.txt',), {}, {'stdout': StdOut(True, 'hw06/in/in40_02.out')}),
    }
    return out


if __name__ == '__main__':
    main(sys.argv[1:])
