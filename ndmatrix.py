import numpy as np


def rec_max(l):
    if isinstance(l, list):
        return max(rec_max(it) for it in l)
    return l


def rec_argmax(l):
    if isinstance(l, list):
        m = -float('inf')
        mi = ()
        for i, it in enumerate(l):
            v, prev = rec_argmax(it)
            if v > m:
                m = v
                mi = (i,) + prev
        return m, mi

    return l, ()


def rec_map(l, func):
    if isinstance(l, list):
        return [rec_map(it, func) for it in l]
    return func(l)


class NDMatrix:

    def __init__(self, shape):
        self.shape = shape
        self.arr = np.zeros(shape, dtype=int).tolist()

    def __len__(self):
        return self.shape[0]

    def max(self):
        return rec_max(self.arr)

    def argmax(self):
        return rec_argmax(self.arr)[1]

    def _valid_key(self, key):
        return len(key) == len(self.shape)

    def __setitem__(self, key, value):
        if not self._valid_key(key):
            pass
        a = self.arr
        for k in key[:-1]:
            a = a[k]
        a[key[-1]] = value

    def __getitem__(self, key):
        if not self._valid_key(key):
            pass
        a = self.arr
        for k in key:
            a = a[k]
        return a

    def __str__(self):
        return str(self.arr)

    def nice_str(self, fermat='{:3d}'):
        h = rec_map(self.arr, fermat.format)
        return '\n'.join(map(' '.join, h))