import re
from collections import Iterable

import numpy as np

from hw04.gap_functions import AbstractGap, LinearGap
from ndmatrix import NDMatrix


def all_element_identical(lst):
    """
    Return true for list (tuple, ...) when all items inside is identical.
    :param lst:
    :return:
    """
    return lst[1:] == lst[:-1]


def first(it: Iterable):
    """
    Return first element of iterable.
    :param it:
    :return:
    """
    return it[0]


def second(it: Iterable):
    """
    Return second element of iterable.
    :param it:
    :return:
    """
    return it[1]


def read_fasta(g: str):
    """
    Return tuple (name, data) for string containing file in fasta format
    :param g:
    :return: [(name, data), ...]
    """
    return map(lambda it: (it[0], ''.join(it[1:])), map(str.split, filter(len, g.split('>'))))


def to_rna(s):
    return s.replace('T', 'U')


def reverse_complement(s: str):
    s = s.lower()
    d = {'a': 'T', 't': 'A', 'c': 'G', 'g': 'C'}
    for k, v in d.items():
        s = s.replace(k, v)

    return ''.join(s[::-1])


def get_longest_orf(s):
    m = get_all_orf(s)
    out = ''
    mmax = 0
    for r in m:
        if mmax < len(r):
            out = r
            mmax = len(r)
    return out


def get_all_orf(s):
    s_rna = to_rna(s)
    q = re.compile('(?=(AUG(...)*?(UAA|UAG|UGA)))')
    m = q.findall(s_rna)
    return map(first, m)


def profile_matrix(arr):
    lst = list(sorted(np.unique(arr)))
    out = np.zeros([len(lst), arr.shape[1]], dtype=int)
    for i, elm in enumerate(lst):
        out[i, :] = (arr == elm).sum(axis=0)
    return lst, out


def convert(l, stop=''):
    """Convert RNA to protein."""
    d = {
        'UUU': 'F', 'CUU': 'L', 'AUU': 'I', 'GUU': 'V',
        'UUC': 'F', 'CUC': 'L', 'AUC': 'I', 'GUC': 'V',
        'UUA': 'L', 'CUA': 'L', 'AUA': 'I', 'GUA': 'V',
        'UUG': 'L', 'CUG': 'L', 'AUG': 'M', 'GUG': 'V',
        'UCU': 'S', 'CCU': 'P', 'ACU': 'T', 'GCU': 'A',
        'UCC': 'S', 'CCC': 'P', 'ACC': 'T', 'GCC': 'A',
        'UCA': 'S', 'CCA': 'P', 'ACA': 'T', 'GCA': 'A',
        'UCG': 'S', 'CCG': 'P', 'ACG': 'T', 'GCG': 'A',
        'UAU': 'Y', 'CAU': 'H', 'AAU': 'N', 'GAU': 'D',
        'UAC': 'Y', 'CAC': 'H', 'AAC': 'N', 'GAC': 'D',
        'UAA': '', 'CAA': 'Q', 'AAA': 'K', 'GAA': 'E',
        'UAG': '', 'CAG': 'Q', 'AAG': 'K', 'GAG': 'E',
        'UGU': 'C', 'CGU': 'R', 'AGU': 'S', 'GGU': 'G',
        'UGC': 'C', 'CGC': 'R', 'AGC': 'S', 'GGC': 'G',
        'UGA': '', 'CGA': 'R', 'AGA': 'R', 'GGA': 'G',
        'UGG': 'W', 'CGG': 'R', 'AGG': 'R', 'GGG': 'G', }
    out = []
    for xyz in zip(l[0::3], l[1::3], l[2::3]):
        x, y, z = xyz
        xyz = x + y + z
        if xyz in d:
            out.append(d[xyz])
    return ''.join(out)


def load_matrix_from_file(fn):
    with open(fn) as f:
        return load_matrix_as_dict(f.readlines())


def load_matrix_as_dict(arr):
    head = arr[0].split()
    arr = arr[1:]
    out = dict()
    for l in arr:
        l = l.split()
        frm = l[0]
        for to, it in zip(head, l[1:]):
            out[frm, to] = int(it)

    return out


def gap_length(move: NDMatrix, i, j, di, dj):
    length = 0
    while True:
        if move[i, j] == (i + di, j + dj):
            length += 1
            i = i + di
            j = j + dj
        else:
            return length


def red(s, t, d: NDMatrix, move: NDMatrix, m=None, n=None):
    if not m:
        m = len(s)
    if not n:
        n = len(t)
    s = list(s)
    t = list(t)
    while True:
        nxt = move[m, n]
        if not nxt:
            break
        nm, nn = nxt
        if nm == m - 1 and nn == n - 1:
            pass
        elif nm == m - 1:
            # print('l')
            t.insert(n, '-')
        elif nn == n - 1:
            # print('u')
            s.insert(m, '-')

        m, n = nm, nn

    print(''.join(s))
    print(''.join(t))


def levenshtein_2d(s, t, scoring_matrix: dict = None, gap_function: AbstractGap = None, sub_start=float('inf')):
    if not gap_function:
        gap_function = LinearGap(1)
    d = NDMatrix([len(s) + 1, len(t) + 1])
    move = NDMatrix([len(s) + 1, len(t) + 1])
    m = len(s)
    n = len(t)

    for i in range(1, m + 1):
        d[i, 0] = -gap_function[i]
        move[i, 0] = (i - 1, 0)

    for j in range(1, n + 1):
        d[0, j] = -gap_function[j]
        move[0, j] = (0, j - 1)

    d[0, 0] = 0

    for j in range(1, n + 1):
        for i in range(1, m + 1):
            if scoring_matrix:
                sc = scoring_matrix[s[i - 1], t[j - 1]]
            else:
                sc = 0 if s[i - 1] == t[j - 1] else 1

            i_gap = gap_length(move, i - 1, j, -1, 0) + 1
            j_gap = gap_length(move, i, j - 1, 0, -1) + 1

            price_arr = [*[(d[i - ii, j] - gap_function[ii], (i - 1, j)) for ii in range(0, i_gap + 1)],
                         *[(d[i, j - jj] - gap_function[jj], (i, j - 1)) for jj in range(0, j_gap + 1)],
                         (d[i - 1, j - 1] + sc, (i - 1, j - 1)),
                         (sub_start, (0, 0))]

            mm = max(price_arr)

            d[i, j] = mm[0]
            move[i, j] = mm[1]

    return d, move


def transition(a, b):
    a, b = sorted([a, b])
    return (a == 'A' and b == 'G') or (a == 'C' and b == 'T')


def transversion(a, b):
    return not transition(a, b) and a != b


def print_2d_dict(d):
    row = sorted(set(map(first, d.keys())))
    col = list(sorted(set(map(second, d.keys()))))
    print('\t'.join(map(str, [''] + col)))
    for r in row:
        print('\t'.join(map(str, [r] + [d.get((c, r), '') for c in col])))


class Infix:
    def __init__(self, function):
        self.function = function

    def __ror__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))

    def __or__(self, other):
        return self.function(other)

    def __rlshift__(self, other):
        return Infix(lambda x, self=self, other=other: self.function(other, x))

    def __rshift__(self, other):
        return self.function(other)

    def __call__(self, value1, value2):
        return self.function(value1, value2)


def rocket(fnc, *op):
    from functools import partial
    return partial(fnc, *op)


O = Infix(rocket)


def add(a, b, c):
    return a + b + c


aa = add | O | 1

print(aa(2, 3))
